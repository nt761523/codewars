//https://www.codewars.com/kata/577bd026df78c19bca0002c0
#include <string>

std::string correct(std::string str){
  //your code here
  for(auto& i:str){
    switch (i){
        case '1':
          i = 'I';
          break;
        case '0':
          i = 'O';
          break;
        case '5':
          i = 'S';
          break;
    }
  }
  return str;
}