//https://www.codewars.com/kata/57eae65a4321032ce000002d
#include <string>

std::string fakeBin(std::string str){
  //your code here
  for(auto& i:str)
    i = (i - '0') < 5 ? '0':'1';
  return str;
}