//https://www.codewars.com/kata/5672a98bdbdd995fad00000f
#include <string>

std::string rps(const std::string& p1, const std::string& p2)
{
    std::string result = "";
    if(p1 == p2){
      result = "Draw!";
    }
    else{
      result = p1 == "rock" && p2 == "scissors" ||
               p1 == "scissors" && p2 == "paper" || 
               p1 == "paper" && p2 == "rock" ? "Player 1 won!" : "Player 2 won!";
    }
    return result;
}