////https://www.codewars.com/kata/515e271a311df0350d00000f
#include <string>

std::string no_space(const std::string& x)
{
  std::string temp = "";
  for(const auto& i: x)
    if(i != ' ') temp += i;
  return temp;
}

// Erase-remove
#include <string>
#include <algorithm>

std::string no_space(const std::string& x)
{
  std::string no_space_str = x;
  no_space_str.erase(std::remove(no_space_str.begin(),no_space_str.end(),' '),no_space_str.end());
  return no_space_str;
}