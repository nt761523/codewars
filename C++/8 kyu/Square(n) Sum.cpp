//https://www.codewars.com/kata/515e271a311df0350d00000f
#include <vector>

int square_sum(const std::vector<int>& numbers)
{
  int sum = 0;
  for(const auto& i:numbers)
    sum += i*i;
    return sum;
}