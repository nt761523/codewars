# CodeWars Solutions

記錄[CodeWars](https://www.codewars.com/) C++的解法

透過[C++程式](https://gitlab.com/nt761523/codewarsautoreadme)自動更新README.md

[![Profile badge](https://www.codewars.com/users/nt761523/badges/large)](https://www.codewars.com/users/nt761523)

## 8 kyu
---
[Convert a Number to a String!](/https://www.codewars.com/kata/5265326f5fda8eb1160004c8/train/cpp)
```cpp
#include <string>

std::string number_to_string(int num) {
  // your code here
  return std::to_string(num);
}
```
[Convert a String to a Number!](/https://www.codewars.com/kata/544675c6f971f7399a000e79/train/cpp)
```cpp
#include <string>

int string_to_number(const std::string& s) {
  //your code here
  return std::stoi(s);
}
```
[Correct the mistakes of the character recognition software](/https://www.codewars.com/kata/577bd026df78c19bca0002c0/train/cpp)
```cpp
#include <string>

std::string correct(std::string str){
  //your code here
  for(auto& i:str){
    switch (i){
        case '1':
          i = 'I';
          break;
        case '0':
          i = 'O';
          break;
        case '5':
          i = 'S';
          break;
    }
  }
  return str;
}
```
[Fake Binary](/https://www.codewars.com/kata/57eae65a4321032ce000002d/train/cpp)
```cpp
#include <string>

std::string fakeBin(std::string str){
  //your code here
  for(auto& i:str)
    i = (i - '0') < 5 ? '0':'1';
  return str;
}
```
[Multiply](/https://www.codewars.com/kata/50654ddff44f800200000004/train/cpp)
```cpp
int multiply(int a, int b)
{
    return a * b;
}
```
[Opposite number](/https://www.codewars.com/kata/56dec885c54a926dcd001095/train/cpp)
```cpp
int opposite(int number) 
{
  return ~number+1;
}
```
[Pillars](/https://www.codewars.com/kata/5bb0c58f484fcd170700063d/train/cpp)
```cpp
long pillars(int num_of_pillars, int distance, int width) {
  return num_of_pillars > 1 ? (num_of_pillars - 1) * (distance * 100) + (num_of_pillars - 2) * width : 0;
}
```
[Remove String Spaces](///https://www.codewars.com/kata/515e271a311df0350d00000f/train/cpp)
```cpp
#include <string>

std::string no_space(const std::string& x)
{
  std::string temp = "";
  for(const auto& i: x)
    if(i != ' ') temp += i;
  return temp;
}

// Erase-remove
#include <string>
#include <algorithm>

std::string no_space(const std::string& x)
{
  std::string no_space_str = x;
  no_space_str.erase(std::remove(no_space_str.begin(),no_space_str.end(),' '),no_space_str.end());
  return no_space_str;
}
```
[Rock Paper Scissors!](/https://www.codewars.com/kata/5672a98bdbdd995fad00000f/train/cpp)
```cpp
#include <string>

std::string rps(const std::string& p1, const std::string& p2)
{
    std::string result = "";
    if(p1 == p2){
      result = "Draw!";
    }
    else{
      result = p1 == "rock" && p2 == "scissors" ||
               p1 == "scissors" && p2 == "paper" || 
               p1 == "paper" && p2 == "rock" ? "Player 1 won!" : "Player 2 won!";
    }
    return result;
}
```
[Square(n) Sum](/https://www.codewars.com/kata/515e271a311df0350d00000f/train/cpp)
```cpp
#include <vector>

int square_sum(const std::vector<int>& numbers)
{
  int sum = 0;
  for(const auto& i:numbers)
    sum += i*i;
    return sum;
}
```
---
